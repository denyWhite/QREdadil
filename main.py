from typing import List, Any
import requests
import sqlite3
from datetime import datetime, timedelta
import csv
import smtplib
from os.path import basename
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from private import private_info


class QRCodes:
    API_URL = "https://ofv-api-v0-1-1.evotor.ru/v1/client/{}/receipts"  # API url
    MAX_PERIOD = 150 * 60  # Максимальный период запроса чеков в секундах

    def __init__(self):
        self._conn = sqlite3.connect('db.sqlite')
        self._cursor = self._conn.cursor()
        self._rules = self.load_rules()

    def __del__(self):
        self._conn.close()

    @staticmethod
    def load_rules_depricated(fname="rules.txt"):
        """
        Загрузка правил из файла
        :fname: имя файла
        """
        f = open(fname)
        rules: List[Any] = list(filter(None, list(map(lambda s: s.strip().lower(), f.readlines()))))
        f.close()
        return rules

    @staticmethod
    def load_rules(fname="rules.txt"):
        """1
        Загрузка правил из файла
        :fname: имя файла
        """
        res = []
        with open(fname, "r") as f_obj:
            for row in csv.reader(f_obj, delimiter=';'):
                if len(row) > 0:
                    res.append(row)
        return res

    def update_tills(self):
        """
        Проверка всех касс
        """
        response = self._cursor.execute("SELECT * FROM tills").fetchall()
        cnt = 0
        for token, fn, dt in response:
            cnt += self.get_checks(token, fn, dt)
        print("Обработано {} чек.".format(cnt))

    @staticmethod
    def int_sum_to_float(fint_sum):
        """
        Преобразование целой суммы в копейках в дробные рубли (1234 в 12.34)
        :param fint_sum: Сумма в копейках
        :return: Сумма в рублях
        """
        return "%.2f" % (int(fint_sum) / 100)

    def get_fns_url(self, check: dict):
        """
        Формирование строки параметров для QR кода
        :param check: Словарь с чеком
        :return: Строка параметров
        """
        dt_obj = datetime.strptime(check["receiptDate"], "%Y.%m.%d %H:%M:%S.%f")
        dt_str = dt_obj.strftime("%Y%m%dT%H%M")
        summ = self.int_sum_to_float(check["totalSum"])
        return "t={0}&s={1}&fn={2}&i={3}&fp={4}&n={5}".format(dt_str, summ,
                                                              check["fiscalDriveNumber"],
                                                              check["fiscalDocumentNumber"],
                                                              check["fiscalSign"], 1)

    @staticmethod
    def date_to_str(dt):
        return dt.strftime("%Y-%m-%d %H:%M:%S")

    @staticmethod
    def str_to_date(strng):
        return datetime.strptime(strng, "%Y-%m-%d %H:%M:%S")

    def till_dt_update(self, token, fn, dt):
        self._cursor.execute("UPDATE tills SET dt=:dt WHERE token=:token AND fn=:fn",
                             {"dt": dt, "token": token, "fn": fn})
        self._conn.commit()

    def get_checks(self, token, fn, dt):
        try:
            date_from: datetime = self.str_to_date(dt)
        except TypeError:
            date_from = self.str_to_date("1900-01-01 00:00:00")
        date_to = datetime.now()
        delta = (date_to - date_from).seconds
        if delta > self.MAX_PERIOD:
            date_from = date_to - timedelta(seconds=self.MAX_PERIOD)
        # date_from = date_to - timedelta(seconds=self.MAX_PERIOD)
        date_from_str = self.date_to_str(date_from)
        date_to_str = self.date_to_str(date_to)

        url = self.API_URL.format(fn)
        response = requests.get(url, headers={"token": token},
                                params={"dateFrom": date_from_str, "dateTo": date_to_str})
        if response.status_code == 200:
            self.till_dt_update(token, fn, date_to_str)
            for check in response.json()["receipts"]:
                self.process_check(check)
                return len(response.json()["receipts"])
        else:
            print(response.text)
        return 0

    def process_check(self, check: dict):
        """

        :type check: dict
        """
        # Если в чеке есть нужные позиции, то фомрируем
        is_good = False
        for item in check["items"]:
            name = item["name"].lower()
            for rule in self._rules:
                if self.is_in_rules(rule, name, int(item["sum"]) // 100):  # Чек нужно сохранять
                    is_good = True
                    break
        if is_good:
            self.save_check(check)

    @staticmethod
    def generate_qr(data, fn="qr.png"):
        text = str(data)
        from qrcode import QRCode, constants
        qr = QRCode(
            version=1,
            error_correction=constants.ERROR_CORRECT_M,
            box_size=3,
            border=2,
        )
        qr.add_data(text)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        img.save(fn)

    @staticmethod
    def save_to_file_depricated(fn, text):
        f = open(fn, 'a')
        f.write(text)
        f.close()

    @staticmethod
    def save_to_file(fn, text):
        f = open(fn, 'a')
        f.close()
        with open(fn, 'r+') as f:
            f.writelines(["{}\n".format(text)] + [f.readlines(), f.seek(0)][0])

    def is_in_rules(self, rule, name, summ):
        q = name.find(rule[0]) != -1
        if len(rule) > 1:
            q = q and summ > int(rule[1])
        return q

    def save_check(self, check: dict):
        """
        Генерация файла index.html
        Генерация куэр кода
        """
        check_html = ""
        for item in check["items"]:
            name = item["name"].lower()
            summ: str = self.int_sum_to_float(item["sum"])
            for rule in self._rules:
                if self.is_in_rules(rule, name, int(item["sum"]) // 100):
                    name = name.replace(rule[0], '<span style="color: red;">{}</span>'.format(rule[0].upper()))
            check_html += "<li>{0} &mdash; {1}</li>".format(name, summ)
        check_id = check["rqId"]
        check_time = check["rqDate"]
        fns_url = self.get_fns_url(check)
        fn = "output/image/{}.png".format(check_id)
        self.generate_qr(fns_url, fn=fn)
        check_html = '<div id="block{1}" class="block" dt="{2}"><h4>{2}</h4>{0}<p><img src="image/{1}.png" style="width: 300px;"></p></div><hr>'.\
            format(check_html, check_id, check_time)
        self.save_to_file("output/index.htm", check_html)
        self.send_mail(check_html, fn)


    def send_mail(self, text, filename=False):
        try:
            me, my_password, you, server = private_info
            msg = MIMEMultipart()
            msg["Subject"] = "Edadil - New bill"
            msg["From"] = me
            msg["To"] = you

            html = "<html><body>{}</body></html>".format(text)
            textpart = MIMEText(html, 'html')
            msg.attach(textpart)

            with open(filename, "rb") as file_d:
                msg.attach(MIMEImage(file_d.read()))

            s = smtplib.SMTP_SSL(server)
            s.login(me, my_password)
            s.sendmail(me, you, msg.as_string())
            s.quit()
        except:
            print("Send message error!")

QRCodes().update_tills()

